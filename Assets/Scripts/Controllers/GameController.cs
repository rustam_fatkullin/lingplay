﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace LingPlay.CarGame
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private InputController _inputController;
        [SerializeField] private UIController _uiController;
        [SerializeField] private Player _player;

        [SerializeField] private Transform _gameOverBottomBorder;
        [SerializeField] private Transform _finishFlag;

        private const float GamePauseTimeScale = 0.0f;
        private const float GameNormalTimeScale = 1.0f;
        private const float MaxTimeNoGroundTouches = 7.0f;

        private bool _isPlaying = false;
        private bool _isWaitingForConfirm = false;
        private float _sessionTime = 0.0f;
        private float _noGroundTouchesTime = 0.0f;
        private List<Elevator> _elevators;

        public event Action<bool> GamePaused;

        public void OnMenuOpen()
        {
            ResetAllObjects();

            Time.timeScale = GamePauseTimeScale;
            _isPlaying = false;

            _uiController.ShowMenu();
            _inputController.IsEnabledGamePlayInput = false;
        }

        public void OnGameStart()
        {
            ResetAllObjects();

            Time.timeScale = GameNormalTimeScale;
            _sessionTime = 0.0f;
            _noGroundTouchesTime = 0.0f;
            _isPlaying = true;
            _isWaitingForConfirm = false;

            _uiController.ShowHud();
            _inputController.IsEnabledGamePlayInput = true;
        }

        public void OnGameExit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }

        private void Awake()
        {
            _elevators = FindObjectsOfType<Elevator>().ToList();

            _inputController.Escape += OnEscape;
            _inputController.Confirm += OnConfirm;

            _player.GroundedChange += OnGroundedChange;

            InitAllObjects();

            OnMenuOpen();
        }

        private void Update()
        {
            _sessionTime += Time.deltaTime;
            _noGroundTouchesTime += Time.deltaTime;

            _uiController.UpdateHud(_sessionTime);

            CheckGameOver();
            CheckFinish();
        }

        private void InitAllObjects()
        {
            _player.Init();
            _elevators.ForEach(item => item.Init());
        }

        private void ResetAllObjects()
        {
            _player.Reset();
            _elevators.ForEach(item => item.Reset());
        }

        private void OnEscape()
        {
            if (!_isPlaying || _isWaitingForConfirm)
                return;

            GamePaused.Dispatch(true);

            _isWaitingForConfirm = true;
            Time.timeScale = GamePauseTimeScale;

            _uiController.EnableConfirmPopup(true);
            _inputController.IsEnabledGamePlayInput = false;
            _inputController.IsEnabledUIInput = true;
        }

        private void OnConfirm(bool agree)
        {
            if (!_isWaitingForConfirm)
                return;

            GamePaused.Dispatch(false);

            _isWaitingForConfirm = false;

            _uiController.EnableConfirmPopup(false);
            _inputController.IsEnabledUIInput = false;
            _inputController.IsEnabledGamePlayInput = true;

            if (agree)
                OnMenuOpen();
            else
                Time.timeScale = GameNormalTimeScale;
        }

        private void OnGroundedChange(bool isGrounded)
        {
            if (isGrounded)
                _noGroundTouchesTime = 0.0f;
        }

        private void CheckGameOver()
        {
            if (!_isPlaying)
                return;

            var isFallingDown = _player.transform.position.y.CompareTo(_gameOverBottomBorder.position.y) <= 0;
            var isLongTimeNoTouches = MaxTimeNoGroundTouches.CompareTo(_noGroundTouchesTime) <= 0;

            if (isFallingDown || isLongTimeNoTouches)
                OnGameStart();
        }

        private void CheckFinish()
        {
            if (!_isPlaying)
                return;

            if (_player.transform.position.x.CompareTo(_finishFlag.position.x) >= 0)
                OnMenuOpen();
        }
    }
}
