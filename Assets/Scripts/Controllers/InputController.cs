﻿using System;
using UnityEngine;


namespace LingPlay.CarGame
{
    public class InputController : MonoBehaviour
    {
        [SerializeField] private KeyCode _forwardAccKey;
        [SerializeField] private KeyCode _backwardAccKey;
        [SerializeField] private KeyCode _flipCode;
        [SerializeField] private KeyCode _escapeCode;
        [SerializeField] private KeyCode _agreeCode;
        [SerializeField] private KeyCode _disagreeCode;

        public event Action<bool> ForwardAcceleration;
        public event Action<bool> Confirm;
        public event Action BackwardAcceleration;
        public event Action Flip;
        public event Action Escape;

        public bool IsEnabledGamePlayInput { get; set; }
        public bool IsEnabledUIInput { get; set; }

        private void Update()
        {
            if (IsEnabledGamePlayInput)
            {
                if (Input.GetKey(_forwardAccKey))
                    ForwardAcceleration.Dispatch(true);

                if (Input.GetKeyUp(_forwardAccKey))
                    ForwardAcceleration.Dispatch(false);

                if (Input.GetKey(_backwardAccKey))
                    BackwardAcceleration.Dispatch();

                if (Input.GetKeyDown(_flipCode))
                    Flip.Dispatch();

                if (Input.GetKeyDown(_escapeCode))
                    Escape.Dispatch();
            }

            if (IsEnabledUIInput)
            {
                if (Input.GetKeyDown(_agreeCode))
                    Confirm.Dispatch(true);

                if (Input.GetKeyDown(_disagreeCode))
                    Confirm.Dispatch(false);
            }
        }
    }
}