﻿using UnityEngine;


namespace LingPlay.CarGame
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private Transform _playerTransform;

        private void Update()
        {
            var pos = transform.position;
            pos.x = _playerTransform.position.x;
            pos.y = _playerTransform.position.y;
            transform.position = pos;
        }
    }
}
