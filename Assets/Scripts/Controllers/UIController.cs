﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace LingPlay.CarGame
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private View _confirmPopup;
        [SerializeField] private Text _hudViewTimeLabel;
        [SerializeField] private List<View> _views;

        private const string TimeLabelPattern = "Время заезда: {0:0.00}";

        public enum ViewType
        {
            Menu,
            Hud,
            ConfirmPopup
        }

        public void ShowMenu()
        {
            ShowView(ViewType.Menu);
        }

        public void ShowHud()
        {
            ShowView(ViewType.Hud);
        }

        public void EnableConfirmPopup(bool isEnable)
        {
            _confirmPopup.gameObject.SetActive(isEnable);
        }

        public void UpdateHud(float sessionTime)
        {
            _hudViewTimeLabel.text = String.Format(TimeLabelPattern, sessionTime);
        }

        private void Awake()
        {
            ShowMenu();
        }

        private void ShowView(ViewType type)
        {
            _views.ForEach(item => item.gameObject.SetActive(item.Type == type));
        }
    }
}