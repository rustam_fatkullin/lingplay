﻿using System;


namespace LingPlay.CarGame
{
    public static class ActionsExtensions
    {
        public static void Dispatch(this Action act)
        {
            if (act != null)
                act();
        }

        public static void Dispatch<T>(this Action<T> act, T paramObj)
        {
            if (act != null)
                act(paramObj);
        }
    }
}
