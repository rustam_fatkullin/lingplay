﻿using UnityEngine;


namespace LingPlay.CarGame
{
    public class Elevator : SoundedObject
    {
        [SerializeField] private GameController _gameController;

        [SerializeField] private Vector2 _topPos;
        [SerializeField] private float _speed;
        [SerializeField] private LayerMask _playerLayer;

        private Vector2 _initPos;
        private Vector2 _targetPos;
        private Vector2 _startPos;

        private float _moveTime;
        private float _currMoveTime;
        private bool _isMoving;
        private bool _isMovingUp = true;
        private bool _isBlockedByPlayer = false;
        private Transform _playerTransform;

        private bool IsOnTop
        {
            get { return Mathf.Approximately((_startPos - _topPos).magnitude, 0.0f); }
        }

        public void TryToLiftUp(Transform player)
        {
            if (_isMoving || IsOnTop)
                return;

            _isMoving = true;
            _isMovingUp = true;
            _currMoveTime = 0.0f;

            _playerTransform = player;
            _playerTransform.parent = transform;

            EnableSoundEffect(true);
        }

        public void TryToLiftDown(Vector2 playerTop)
        {
            var isPlayerOnTop = transform.position.y.CompareTo(playerTop.y) <= 0;

            if (_isMoving || !IsOnTop || isPlayerOnTop)
                return;

            _isMoving = true;
            _isMovingUp = false;
            _currMoveTime = 0.0f;
            EnableSoundEffect(true);
        }

        public override void Init()
        {
            base.Init();

            _gameController.GamePaused += OnGamePaused;
            _initPos = transform.position;

            Reset();
        }

        public void Reset()
        {
            transform.position = _initPos;
            _startPos = _initPos;
            _targetPos = _topPos;

            _isMoving = false;
            _currMoveTime = 0;
            _moveTime = (_targetPos.y - _startPos.y)/_speed;

            EnableSoundEffect(false);
        }

        private void Update()
        {
            MovingStep();
        }

        private void OnTriggerEnter2D(Collider2D detectedCollider)
        {
            if (!_isMoving || _isMovingUp)
                return;

            if (1 << detectedCollider.gameObject.layer == _playerLayer)
                _isBlockedByPlayer = true;
        }

        private void OnTriggerExit2D(Collider2D detectedCollider)
        {
            if (!_isMoving || _isMovingUp)
                return;

            if (1 << detectedCollider.gameObject.layer == _playerLayer)
                _isBlockedByPlayer = false;
        }

        private void MovingStep()
        {
            if (!_isMoving || _isBlockedByPlayer)
                return;

            _currMoveTime += Time.deltaTime;

            var isMovingFinished = _currMoveTime.CompareTo(_moveTime) >= 0;

            _currMoveTime = Mathf.Min(_currMoveTime, _moveTime);
            transform.position = _startPos + (_targetPos - _startPos)*(_currMoveTime/_moveTime);

            if (isMovingFinished)
                OnMoveFinished();
        }

        private void OnMoveFinished()
        {
            _isMoving = false;

            if (_playerTransform != null)
                _playerTransform.parent = null;

            SwapStartAndTargetPos();
            EnableSoundEffect(false);
        }

        private void SwapStartAndTargetPos()
        {
            var tmp = _startPos;
            _startPos = _targetPos;
            _targetPos = tmp;
        }

        private void OnGamePaused(bool isPaused)
        {
            PauseSoundEffect(isPaused);
        }
    }
}
