﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace LingPlay.CarGame
{
    public class Player : SoundedObject
    {
        [SerializeField] private GameController _gameController;
        [SerializeField] private InputController _inputController;

        [SerializeField] private float _carAccelerationForce;

        [SerializeField] private GameObject _leftDirSprite;
        [SerializeField] private GameObject _rightDirSprite;

        [SerializeField] private Transform _wheelL;
        [SerializeField] private Transform _wheelR;

        [SerializeField] private Transform _bottomL;
        [SerializeField] private Transform _bottomR;

        [SerializeField] private LayerMask _elevatorMask;
        [SerializeField] private LayerMask _groundMask;
        [SerializeField] private float _needElevatorCheckRadius;

        private const float CarStopThreshold = 0.002f;
        private const float GroundCheckRadius = 0.05f;

        private float _flipped = 1.0f;
        private Vector2 _initPos;
        private bool _isGrounded = true;
        private Rigidbody2D _carBody;
        private List<Rigidbody2D> _wheels = new List<Rigidbody2D>();

        private bool IsFlipped
        {
            get { return _flipped < 0.0f; }
        }

        private Vector2 ToForwardForce
        {
            get { return Vector2.right*_flipped*_carAccelerationForce; }
        }

        public event Action<bool> GroundedChange;

        public override void Init()
        {
            base.Init();

            _carBody = GetComponent<Rigidbody2D>();
            _wheels.Add(_wheelL.GetComponent<Rigidbody2D>());
            _wheels.Add(_wheelR.GetComponent<Rigidbody2D>());

            _initPos = transform.position;

            _gameController.GamePaused += OnGamePaused;
            _inputController.ForwardAcceleration += OnForwardAcceleration;
            _inputController.BackwardAcceleration += OnBackwardAcceleration;
            _inputController.Flip += OnFlip;

            Reset();
        }

        public void Reset()
        {
            transform.position = _initPos;
            transform.rotation = Quaternion.identity;

            if (IsFlipped)
                OnFlip();

            _carBody.Sleep();
            _wheels.ForEach(item => item.Sleep());

            EnableSoundEffect(false);
        }

        private void Update()
        {
            CheckEnterToElevator();
            CheckNeedElevators();
            CheckGrounded();
        }

        private void OnFlip()
        {
            _flipped *= -1;

            _rightDirSprite.SetActive(!IsFlipped);
            _leftDirSprite.SetActive(IsFlipped);
        }

        private void OnForwardAcceleration(bool isPressed)
        {
            EnableSoundEffect(isPressed);

            if (isPressed && _isGrounded)
                _carBody.AddForce(ToForwardForce);
        }

        private void OnBackwardAcceleration()
        {
            if (!_isGrounded)
                return;

            if (_flipped*_carBody.velocity.x.CompareTo(CarStopThreshold) > 0)
                _carBody.AddForce(-ToForwardForce);
            else
                _carBody.velocity = Vector2.zero;
        }
       
        private void CheckEnterToElevator()
        {
            var colliders = RayCastWheels(_elevatorMask);
            var isOnElevator = colliders.Count(item => item != null) == 2;

            if (isOnElevator)
                colliders.First().GetComponent<Elevator>().TryToLiftUp(transform);
        }

        private void CheckGrounded()
        {
            _isGrounded = RayCastWheels(_groundMask).Count(item => item != null) != 0;

            GroundedChange.Dispatch(_isGrounded);
        }

        private IEnumerable<Collider2D> RayCastWheels(LayerMask mask)
        {
            var lDir = (_bottomL.position - _wheelL.position).normalized;
            var rDir = (_bottomR.position - _wheelR.position).normalized;

            var leftWheelCollision = Physics2D.Raycast(_bottomL.position, lDir, GroundCheckRadius, mask);
            var rightWheelCollision = Physics2D.Raycast(_bottomR.position, rDir, GroundCheckRadius, mask);

            return new List<Collider2D>() {leftWheelCollision.collider, rightWheelCollision.collider};
        }

        private void CheckNeedElevators()
        {
            var elevators = Physics2D.OverlapCircleAll(transform.position, _needElevatorCheckRadius, _elevatorMask);

            foreach (var elv in elevators)
                elv.GetComponent<Elevator>().TryToLiftDown(transform.position);
        }

        private void OnGamePaused(bool isGamePaused)
        {
            if (isGamePaused)
                EnableSoundEffect(false);
        }
    }
}
