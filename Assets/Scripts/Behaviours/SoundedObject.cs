﻿using UnityEngine;


namespace LingPlay.CarGame
{
    public class SoundedObject : MonoBehaviour
    {
        private AudioSource _soundEffectAudioSource;
        private bool _isNeedToRestore = false;


        public virtual void Init()
        {
            _soundEffectAudioSource = GetComponent<AudioSource>();
        }

        public void PauseSoundEffect(bool isPaused)
        {
            if (isPaused)
            {
                if (_soundEffectAudioSource.isPlaying)
                {
                    _soundEffectAudioSource.Pause();
                    _isNeedToRestore = true;
                }
            }
            else
            {
                if (_isNeedToRestore)
                {
                    _soundEffectAudioSource.Play();
                    _isNeedToRestore = false;
                }
            }
        }

        protected void EnableSoundEffect(bool isEnabled)
        {
            if (isEnabled)
            {
                if (!_soundEffectAudioSource.isPlaying)
                {
                    _soundEffectAudioSource.time = 0.0f;
                    _soundEffectAudioSource.Play();
                }
            }
            else
            {
                _soundEffectAudioSource.Stop();
            }
        }
    }
}
