﻿using UnityEngine;


namespace LingPlay.CarGame
{
    public class View : MonoBehaviour
    {
        [SerializeField] private UIController.ViewType _type;

        public UIController.ViewType Type
        {
            get { return _type; }
        }
    }
}
